<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod
 * @subpackage vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_vote_activity_task
 */

/**
 * Define the complete vote structure for backup, with file and id annotations
 */
class backup_vote_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo.
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated.
        $vote = new backup_nested_element('vote', array('id'), array(
            'name', 'intro', 'introformat', 'timecreated', 'timemodified', 'votetype', 'closedate',
            'displayinline', 'votestate', 'completionvoted'));

        $questions = new backup_nested_element('questions');

        $question = new backup_nested_element('question', array('id'), array(
            'question', 'sortorder'));

        $options = new backup_nested_element('options');

        $option = new backup_nested_element('option', array('id'), array(
            'questionid', 'optionname', 'sortorder'));

        $votevotes = new backup_nested_element('vote_votes');

        $votevote = new backup_nested_element('vote_vote', array('id'), array(
            'optionid', 'userid', 'vote'));

        // Build the tree.
        $vote->add_child($questions);
        $questions->add_child($question);

        $vote->add_child($options);
        $options->add_child($option);

        $vote->add_child($votevotes);
        $votevotes->add_child($votevote);

        // Define sources.
        $vote->set_source_table('vote', array('id' => backup::VAR_ACTIVITYID));

        $question->set_source_table('vote_question', array('voteid' => backup::VAR_PARENTID));

        $option->set_source_table('vote_options', array('voteid' => backup::VAR_PARENTID));

        // All the rest of elements only happen if we are including user info.
        if ($userinfo) {
            $votevote->set_source_table('vote_votes', array('voteid' => backup::VAR_PARENTID));
        }

        // Define id annotations.
        $votevote->annotate_ids('user', 'userid');

        // Define file annotations.

        // Return the root element (vote), wrapped into standard activity structure.
        return $this->prepare_activity_structure($vote);
    }
}
