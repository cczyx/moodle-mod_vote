@mod @mod_vote @uon
Feature: Vote secrecy
    While a vote is active to maintain secrecy
    As a student
    I should not be able to see the results.

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student1@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        # votetype 2 is a vote.
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Vote test | Votes for cash | 2 | 1 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |
            | vote1 | What colour is the sky? | Blue |

    Scenario: A student should not be able to see the results while voting is not closed.
        Given I log in as "student1"
        And I follow "Course 1"
        And I follow "Vote test"
        When I fill in the "Vote test" vote with:
            | question | vote |
            | What colour is the sky? | Blue |
        Then I should see "Thank you for voting"
        But I should not see "Blue (1 votes)"
        And I should not see "Green (0 votes)"
        # Ensure that leaving the vote and going back in does not let you see the results.
        When I follow "Course 1"
        And I follow "Vote test"
        Then I should see "Thank you for voting"
        But I should not see "Blue (1 votes)"
        And I should not see "Green (0 votes)"
