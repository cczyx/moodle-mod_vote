@mod @mod_vote @uon
Feature: Vote activity creation
    In order to use the activity
    As an editing teacher
    I need to be create a vote

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | testteacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |

    Scenario: Create a poll
        Given I log in as "teacher1"
        And I follow "Course 1"
        And I turn editing mode on
        And I add a "Vote" to section "1" and I fill the form with:
            | Vote name | The title of my vote |
            | Description | Description of the vote activity I will create |
            | id_votetype_1 | Poll |
            | Vote active | No |
        And I follow "The title of my vote"
        Then I should see "Vote type: Poll"
        But I should not see "Vote type: Vote"
        And I should not see "Vote type: Alternative vote"

    Scenario: Create a poll
        Given I log in as "teacher1"
        And I follow "Course 1"
        And I turn editing mode on
        And I add a "Vote" to section "1" and I fill the form with:
            | Vote name | The title of my vote |
            | Description | Description of the vote activity I will create |
            | id_votetype_2 | Vote |
            | Vote active | No |
        And I follow "The title of my vote"
        Then I should see "Vote type: Vote"
        But I should not see "Vote type: Poll"
        And I should not see "Vote type: Alternative vote"

    Scenario: Create a poll
        Given I log in as "teacher1"
        And I follow "Course 1"
        And I turn editing mode on
        And I add a "Vote" to section "1" and I fill the form with:
            | Vote name | The title of my vote |
            | Description | Description of the vote activity I will create |
            | id_votetype_3 | Alternative vote |
            | Vote active | No |
        And I follow "The title of my vote"
        Then I should see "Vote type: Alternative vote"
        But I should not see "Vote type: Poll"
        And I should not see "Vote type: Vote"
