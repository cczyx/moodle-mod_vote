@mod @mod_vote @uon
Feature: Inactive votes
    In order to allow votes to be edited safely
    As a student
    I should not be able to see the options.

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | student1 | Student | 1 | student1@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | student1 | C1 | student |
        # votesate 0 is not active
        And the following "activities" exist:
            | activity | course | idnumber | name | intro | votetype | votestate |
            | vote | C1 | vote1 | Poll test | My little polly | 1 | 0 |
        And the following vote "questions" exist:
            | vote | question |
            | vote1 | What colour is the sky? |
        And the following vote "options" exist:
            | vote | question | optionname |
            | vote1 | What colour is the sky? | Green |

    Scenario: Inactive polls are not avaliable to students
        Given I log in as "student1"
        And I follow "Course 1"
        When I follow "Poll test"
        Then I should see "Voting is not open right now, please come back later."
