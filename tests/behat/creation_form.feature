@mod @mod_vote @uon
Feature: Vote activity editing form
    In order to create a vote
    As an editing teacher
    I need to be informed of what information is expected.

    Background:
        Given the following "users" exist:
            | username | firstname | lastname | email |
            | teacher1 | Teacher | 1 | testteacher@example.com |
        And the following "courses" exist:
            | fullname | shortname | category |
            | Course 1 | C1 | 0 |
        And the following "course enrolments" exist:
            | user | course | role |
            | teacher1 | C1 | editingteacher |

    Scenario: Message when saving a vote without completing mandatory field
        Given I log in as "teacher1"
        And I follow "Course 1"
        And I turn editing mode on
        And I add a "Vote" to section "1"
        When I click on "submitbutton" "button"
        Then I should see "You must supply a value here"

    @javascript
    Scenario: Viewing help text
        Given I log in as "teacher1"
        And I follow "Course 1"
        And I turn editing mode on
        And I add a "Vote" to section "1"
        When I click on "Help with Vote name" "link"
        Then I should see "This is the content of the help tooltip associated with the votename field. Markdown syntax is supported." in the ".moodle-dialogue-bd" "css_element"
        When I click on "Help with Close date" "link"
        Then I should see "Sets the last date that votes will be accepted." in the ".moodle-dialogue-bd" "css_element"
        When I click on "Help with Vote type" "link"
        Then I should see "Sets how the vote will behave." in the ".moodle-dialogue-bd" "css_element"
