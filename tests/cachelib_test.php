<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Tests the vote activities mod_vote_cachelib.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 * @group uon
 */
class mod_vote_cachelib_testcase extends advanced_testcase {
    /**
     * Tests that mod_vote_cachelib is able to retrive data correctly.
     *
     * @covers mod_vote_cachelib::get_cached_results
     * @covers mod_vote_cachelib::clear_cache
     * @group mod_vote
     * @group uon
     */
    public function test_get_cached_results() {
        global $DB;

        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');

        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user();

        // Create a course and add a vote activity to it.
        $course0 = self::getDataGenerator()->create_course();
        $vote0 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_POLL));
        $question0 = $votegenerator->create_question(
                $vote0,
                array('question' => 'Test question'),
                array(
                    array('optionname' => 'First option'),
                    array('optionname' => 'Second option'),
                    array('optionname' => 'Third option'),
                    array('optionname' => 'Forth option'),
                ));

        // Enrol some users onto the course and get them to vote in the poll.
        self::getDataGenerator()->enrol_user($user0->id, $course0->id);
        self::getDataGenerator()->enrol_user($user1->id, $course0->id);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id);
        self::getDataGenerator()->enrol_user($user3->id, $course0->id);
        self::getDataGenerator()->enrol_user($user4->id, $course0->id);

        $votegenerator->create_votes($user0, $question0, array($question0->options[2]));
        $votegenerator->create_votes($user1, $question0, array($question0->options[0]));
        $votegenerator->create_votes($user2, $question0, array($question0->options[0]));
        $votegenerator->create_votes($user3, $question0, array($question0->options[1]));
        $votegenerator->create_votes($user4, $question0, array($question0->options[3]));

        // Get the cached results.
        $results = mod_vote_cachelib::get_cached_results(new mod_vote_renderable($vote0->id));

        // Check that the results are correct.
        $this->assertTrue($results->valid());
        $this->validate_result(2, 0, $question0->options[0], $question0, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 0, $question0->options[3], $question0, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 0, $question0->options[1], $question0, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 0, $question0->options[2], $question0, $results->current());
        $results->next();
        $this->assertFalse($results->valid());
        $results->close();

        // Now test AV.
        $vote1 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_AV));
        $question1 = $votegenerator->create_question(
                $vote1,
                array('question' => 'Test question 2'),
                array(
                    array('optionname' => 'Option 1'),
                    array('optionname' => 'Option 4'),
                    array('optionname' => 'Option 3'),
                    array('optionname' => 'Option 2'),
                ));

        $votegenerator->create_votes($user0, $question1, array($question1->options[0], $question1->options[2]));
        $votegenerator->create_votes($user1, $question1, array($question1->options[1]));
        $votegenerator->create_votes($user2, $question1, array($question1->options[2]));
        $votegenerator->create_votes($user3, $question1, array($question1->options[3], $question1->options[2]));
        $votegenerator->create_votes($user4, $question1, array($question1->options[1]));

        // Get the cached results.
        $results = mod_vote_cachelib::get_cached_results(new mod_vote_renderable($vote1->id));

        // Check that the results are correct.
        $this->assertTrue($results->valid());
        $this->validate_result(3, 3, $question1->options[2], $question1, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question1->options[1], $question1, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 2, $question1->options[3], $question1, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 1, $question1->options[0], $question1, $results->current());
        $results->next();
        $this->assertFalse($results->valid());
        $results->close();

        // Create an AV vote with two questions.
        $vote2 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_AV));
        $question2 = $votegenerator->create_question(
                $vote2,
                array('question' => 'Test question 3'),
                array(
                    array('optionname' => 'Option 1', 'sortorder' => 1), // Also testing sort order.
                    array('optionname' => 'Option 4', 'sortorder' => 4),
                    array('optionname' => 'Option 3', 'sortorder' => 3),
                    array('optionname' => 'Option 2', 'sortorder' => 2),
                ));
        $question3 = $votegenerator->create_question(
                $vote2,
                array('question' => 'Test question 4'),
                array(
                    array('optionname' => 'Option 1', 'sortorder' => 4), // Also testing sort order.
                    array('optionname' => 'Option 4', 'sortorder' => 1),
                    array('optionname' => 'Option 3', 'sortorder' => 2),
                    array('optionname' => 'Option 2', 'sortorder' => 3),
                ));

        // Should give the same results as the previous test, even when votes are cast by different people.
        $votegenerator->create_votes($user1, $question2, array($question2->options[0], $question2->options[2]));
        $votegenerator->create_votes($user0, $question2, array($question2->options[1], $question2->options[2], $question2->options[0]));
        $votegenerator->create_votes($user2, $question2, array($question2->options[2], $question2->options[3]));
        $votegenerator->create_votes($user4, $question2, array($question2->options[3], $question2->options[2]));
        $votegenerator->create_votes($user3, $question2, array($question2->options[1], $question2->options[2]));

        // Test when some options are not represented as a first choice.
        $votegenerator->create_votes($user0, $question3, array($question3->options[0], $question3->options[2]));
        $votegenerator->create_votes($user1, $question3, array($question3->options[1]));
        $votegenerator->create_votes($user2, $question3, array($question3->options[0]));
        $votegenerator->create_votes($user3, $question3, array($question3->options[3], $question3->options[2]));
        $votegenerator->create_votes($user4, $question3, array($question3->options[1]));

        // Get the cached results.
        $results = mod_vote_cachelib::get_cached_results(new mod_vote_renderable($vote2->id));

        // Check that the results are correct.
        $this->assertTrue($results->valid());
        $this->validate_result(3, 3, $question2->options[2], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question2->options[1], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 2, $question2->options[3], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 1, $question2->options[0], $question2, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question3->options[1], $question3, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(2, 3, $question3->options[0], $question3, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(1, 2, $question3->options[3], $question3, $results->current());
        $results->next();
        $this->assertTrue($results->valid());
        $this->validate_result(0, 1, $question3->options[2], $question3, $results->current());
        $results->next();
        $this->assertFalse($results->valid());
        $results->close();

        // Test that clearing the cache works, first count the records present.
        $cacherecords = $DB->count_records('vote_result_cache');
        $vote2cahcherecords = $DB->count_records('vote_result_cache', array('voteid' => $vote2->id));
        $this->assertGreaterThan(0, $DB->count_records('vote_result_cache', array('voteid' => $vote2->id)));

        mod_vote_cachelib::clear_cache($vote2->id);
        $this->assertEquals(0, $DB->count_records('vote_result_cache', array('voteid' => $vote2->id)));
        $this->assertEquals(($cacherecords - $vote2cahcherecords), $DB->count_records('vote_result_cache'));

        $this->assertDebuggingNotCalled();
    }

    /**
     * Test that the cached result is as expected.
     *
     * @param int $expectedresult - The number of votes we expect the option to have.
     * @param int $expectedround - The round number we expect the result to have.
     * @param stdClass $expectedoption - The option we expect this result to be for.
     * @param stdClass $expectedquestion - The question we expect this result to be for.
     * @param stdClass $result - The result we are testing.
     */
    protected function validate_result($expectedresult, $expectedround, stdClass $expectedoption, stdClass $expectedquestion, stdClass $result) {
        $this->assertAttributeEquals($expectedoption->optionname, 'optionname', $result);
        $this->assertAttributeEquals($expectedoption->id, 'oid', $result);
        $this->assertAttributeEquals($expectedquestion->id, 'qid', $result);
        $this->assertAttributeEquals($expectedquestion->question, 'question', $result);
        $this->assertAttributeEquals($expectedresult, 'result', $result);
        $this->assertAttributeEquals($expectedround, 'round', $result);
    }
}
